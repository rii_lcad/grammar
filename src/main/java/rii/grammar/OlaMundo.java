package rii.grammar;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OlaMundo {

	@RequestMapping("/")
	@ResponseBody
	public Resposta index() {
		return new Resposta("Envio dos resultados");
	}
}

package rii.grammar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrammarServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrammarServiceApplication.class, args);
	}
}

package rii.grammar;

public class Resposta {
	private String texto;

	public Resposta(String texto) {
		super();
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
